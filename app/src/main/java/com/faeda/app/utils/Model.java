package com.faeda.app.utils;

import android.util.Log;

import java.util.ArrayList;

/**
 * Created by jain on 30-Mar-17.
 */
public class Model {

    public static final int TEXT_TYPE=0;
    public static final int IMAGE_TYPE=1;

    public int type;
    public ArrayList data;

    public Model(int type, ArrayList<String> data)
    {
        this.type=type;
        this.data=data;
        Log.e("FROM MODEL", data.toString());
    }
}