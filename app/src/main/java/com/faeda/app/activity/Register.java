package com.faeda.app.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.faeda.app.R;
import com.faeda.app.utils.Tools;


public class Register extends AppCompatActivity {

    private View parent_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        parent_view = findViewById(android.R.id.content);

        Tools.setSystemBarColor(this, android.R.color.white);
        Tools.setSystemBarLight(this);

        ((View) findViewById(R.id.sign_in)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(parent_view, "Sign up for an account", Snackbar.LENGTH_SHORT).show();
                Intent intent = new Intent(Register.this, Login.class);
                intent.putExtra("activity", "Register");
                startActivity(intent);
            }
        });
    }
}
