package com.faeda.app.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.faeda.app.R;
import com.faeda.app.utils.Tools;

import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;

public class Detail extends AppCompatActivity {
    TextView titledetail, locationdetail, descriptiondetail, numberpeopledetail, pricedetail, categorydetail, datesdetail, detailpenawar;
    Button buttonDetailBantu;
    ArrayList<String> data = new ArrayList<String>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        initToolbar();
        titledetail = (TextView) findViewById(R.id.textDetailTitle);
        numberpeopledetail = (TextView) findViewById(R.id.textDetailJumlahOrang);
        pricedetail = (TextView) findViewById(R.id.textDetailPrice);
        categorydetail = (TextView) findViewById(R.id.textDetailCategory);
        datesdetail = (TextView) findViewById(R.id.textDetailDate);
        locationdetail = (TextView) findViewById(R.id.textDetailAlamat);
        descriptiondetail = (TextView) findViewById(R.id.textDetailDescription);
        buttonDetailBantu = (Button) findViewById(R.id.buttonDetailBantu);
        detailpenawar = (Button) findViewById(R.id.buttonDetailListPenawar);
        Intent intent = getIntent();
        data = intent.getStringArrayListExtra("dataarraylist");
        final String id = data.get(0);
        String title = data.get(1);
        String description = data.get(2);
        String image_thread = data.get(3);
        String location = data.get(4);
        String date_start = data.get(5);
        String date_end = data.get(6);
        String reward_type = data.get(7);
        String price = data.get(8);
        String category = data.get(9);
        String number_people = data.get(10);

        titledetail.setText(title);
        numberpeopledetail.setText(number_people + " orang");
        pricedetail.setText("Rp "+price+".00");
        categorydetail.setText(category);
        locationdetail.setText(location);
        descriptiondetail.setText(description);
        String tanggal = "";
        if(date_start==date_end)
        {
            tanggal = date_start;
        }else{
            tanggal = date_start + " - " + date_end;
        }
        datesdetail.setText(tanggal);

        buttonDetailBantu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("INI ID", id);
                RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
                buttonDetailBantu.setEnabled(false);
                buttonDetailBantu.setAlpha(.5f);
                buttonDetailBantu.setText("Menunggu Persetujuan");
                // on click event open another activity or something
                final String url = "http://instasos.net/insertBantu.php?id_user=1&id_thread="+id;

                JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                        new Response.Listener<JSONObject>()
                        {
                            @Override
                            public void onResponse(JSONObject response) {
                                // display response
                                Log.d("Response", response.toString());



                            }
                        },
                        new Response.ErrorListener()
                        {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d("Error.Response", error.toString());
                            }
                        }
                );

// add it to the RequestQueue
                queue.add(getRequest);
            }
        });
        detailpenawar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("INI ID", id);
                Intent x = new Intent(getApplicationContext(), ListPenawar.class);
                startActivity(x);
            }
        });

    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_menu);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Tools.setSystemBarColor(this, R.color.grey_900);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_basic, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else {
            Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }
}
